const assert = require('assert');

describe('navigation tests', () => {
  it('loads the homepage', () => {
    browser.url("http://localhost:3000/");
    browser.maximizeWindow()
    browser.setTimeout({'pageLoad' :3000},{'implicit':300})
    
    let HomeButton =$('=Home')
    HomeButton.click()
    browser.pause(3000);

    let AboutButton =$('=About')
    AboutButton.click()
    browser.pause(3000);

    let DemoButton =$('=Request a Demo')
    DemoButton.click()
    browser.pause(3000);

  });
});
